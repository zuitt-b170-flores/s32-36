// set up dependencies
const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") //used to encrypt user passwords

/*
import the necessary models that you will need to perform CRUD operations
*/

/*
ACTIVITY s34
	1. find the user in the database	
	   find out if the user is admin

	2. if the user is not an admin, send the response "You are not an admin"

	3. if the user is an admin, create the new Course object
		-save the object
			-if there are errors, return false
			-if there are no errors, return "Course created successfully"
*/

// Course creation
module.exports.addCourse = (reqBody, userData) =>{
	return User.findById(userData.userId).then(result =>{
		if(userData.isAdmin === false){
			return "You are not an admin"
		}else {
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save().then((course, error) => {
		if (error){
			return false
		}else{
			return "Course created successfully"
		}
	})
}
});
}

// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then((result, error) => {
		if (error){
			return false
		}else{
		return result
		}
	})
}

/*
	using the course id (params) in the url, retrieve a course using a get request
		send the code snippet in the batch google chat
*/

module.exports.getCourse = (reqParams) =>{
	console.log(reqParams.courseId);
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

// retrieve all active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then((result,error) => {
		if (error){
		console.log(error)
	}else{
		return result
	}
	})
}

// update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdandUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		// the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result,error) => {
		if (error){
			return false
		}else{
			return true
		}
	})
}

// ACTIVITY 
// archive a course
/*
	1. create an updateCourse object with the content from the reqBody
		reqBody should have the isActive status of the course to be set to false
	2. find the course by its id and update with the updateCourse object using the findByIdandUpdate
		handle the errors that may arise
		error/s - false
		no errors - true
*/

// archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	}

/*
if you do not want to require any request body:
module.exports.archivedCourse = (reqParams)=>{
	let updatedCourse = {
		isActive: false
	}
}
*/
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((result,error) => {
		if (error){
			return false
		}else{
			return true
		}
	})
}
