// set up dependencies
const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") //used to encrypt user passwords

// check if the email exists
/*
	1. check for the email in the database
	2. send the result as a response (with error handling)
*/
/*
	it is conventional for the devs to use Boolean is sending return responses esp with the backend application
*/
module.exports.checkEmail = (requestBody) => {
	return User.find({ email:requestBody.email }).then((result, error) => {
		if (error){
			console.log(error)
			return false
		}else{
			if (result.length > 0){
				// return result
				return true
			}else{
				// return res.send("email does not exist")
				return false
			}
		}
	})
}
/*
USER REGISTRATION
1. create a new User object with the information from the requestBody
2. make sure that the password is encrypted
3. save the new user
*/ 
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the password
			// 10 is the number of rounds/times it runs the algorithm to the reqBody.password
				 // max 72 implementations
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((saved, error) => {
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// USER LOGIN
/*
1. find if the email is existing in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
	return User.findOne( {email: reqBody.email}).then(result => {
		if (result === null){
			return false
		}else{
			// compareSync function - used to compare a non-encrypted password to an encrypted password and returns a Boolean response depending on the result
			/*
			What should we do after the comparison?
				true - a token should be created since the user is existing and the password is correct
				false - the passwords do not match, thus a token should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
				// auth - imported auth.js
					// createAccessToken - function inside the auth.js to create access token
					// .toObject - transforms the User into an Object that can be used by our createAccessToken
			}else{
				return false
			}
		}
	})
}

/*
miniactivity
	create getProfile function inside the userController
	1. find the id of the user using "data" as parameters
	2. if it does not exist, return false,
	3. otherwise, return the details (stretch - make the password invisible)
*/
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		if (result === null){
			return false
		} else {
			result.password = "";
			return result
		}
	})
}

/*
1. find the user/document in the database
2. add the courseId to the user's enrollment array
3. save the document in the database
*/
module.exports.enroll = async (data) => {
	// finding the user in the database
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollment.push({courseId: data.courseId})

		// saving in the database
		return user.save().then((user, err) => {
			if(err){
				return false
			}else {
				return true
			}
		})
	})

	/*
		use another await keyword to updated the enrollees array in the course collection

			find the courseId from the requestBody
			push the userId of the enrollee in the enrollees array of the course
			update the document in the database

			if both pushing are successful, return true
			if both pushing are not successful, return false

	*/
let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding userId in the course enrollees array
		course.enrollees.push({userId: data.userId})
		// saving to database
		return course.save().then((course, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	if (isUserUpdated && isCourseUpdated){
		return true
	}
	else{
		return false
	}
}





/*
Deploying a server

1. register in Heroku (https://signup.heroku.com/login)
2. check if you have heroku in your device
3. create a "Procfile" file
	- it should have P
	- it should not have any file extension
	- inserting the text "web.node app"
4. login your heroku through the git bash/terminal
	- "heroku login"
		press any key
		log in using your heroku account
		there should be confirmation
5. heroku create to create/deploy your server app to a host in the internet
6. git push heroku master to push the updates in heroku
*/