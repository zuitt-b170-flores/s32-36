const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

/*
	Mini activity
	create a function with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController
	REMINDER that we have a parameter needed in the checkEmail function and that is data from request 

	send a screenshot of codes in our Batch Google Chat, once you are done
*/
// checking of email
router.post("/checkEmail", (req,res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

// user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// user Login
router.post("/login", (req,res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})

// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res) => {
	// decode - decrypts the token inside the authorization (which is in the headers of the request)
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
})	

/*
miniactivity
	create a post request under the '/enroll' endpoint that would allow the use of "enroll" function in the userController
	check if the user is logged in
	create a variable and store an object with userId and courseId as fields
		userId - decoded token of the user
		courseId - the course id present in the request body

	use the variable as a parameter of the enrol function

	send a screenshot of your codes in our batch google chat
*/

/*
do not use PATCH because it does not update the time frame
*/
router.post("/enroll", auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result))
})


module.exports = router;

