/*
	user schema:
		REQUIRED FIELDS
		firstName - string
		lastName - string
		age - string
		gender - string
		email - string
		password - string
		mobileNo - string

		Not Required, but has default
		isAdmin - Boolean, false

		enrollment array - will show the courses (id) where the students are enrolled and the date of their enrollment

		push the updates on your s32-36 git repo with the commit message "add user schema"
		add it on Boodle (WDC028 Express.js - API Development Part 1)

*/

const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "First Name is required"]
	},
	lastName:{
		type: String,
		required: [true, "Last Name is required"]
	},
	age:{
		type: String,
		required: [true, "Age is required"]
	},
	gender:{
		type: String,
		required: [true, "Gender is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo:{
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	enrollment:[
	{
		courseId:{
			type: String,
			required: [true, "Course ID is required"]
		},
		enrolledOn:{
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Enrolled"

		}
	}
	]	
})

module.exports = mongoose.model("User", userSchema);